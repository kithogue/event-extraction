from typing import Dict

from torch.utils.data import DataLoader

from RAMS.train.dataset_reader import DatasetReader
from RAMS.train.model import RobertaCRF
from datasets import load_dataset
from torch import cuda
from transformers import Trainer, XLMRobertaTokenizer, XLMRobertaConfig, TrainingArguments, \
    AutoModelForTokenClassification, XLMRobertaTokenizerFast
from model import RobertaForEventClassification, RobertaCRF
from transformers import DataCollatorForTokenClassification

"""Device"""
device = 'cuda' if cuda.is_available() else 'cpu'
print(f"Running on {device}")

"""Hyperparameters"""
MAX_LEN = 512
TRAIN_BATCH_SIZE = 4
VALID_BATCH_SIZE = 2
EPOCHS = 1
LEARNING_RATE = 1e-05
MAX_GRAD_NORM = 10

"""Model and dataset"""
model_name = "xlm-roberta-base"
cache_dir = "RAMS/cache"
ds_reader = DatasetReader()
ds = ds_reader.prepare_datasets()
train = ds['train']
dev = ds['dev']
labels = ds_reader.get_dictionary()
num_labels = len(labels)
label_map: Dict[int, str] = {i: label for i, label in enumerate(labels)}


config = XLMRobertaConfig.from_pretrained(
    model_name,
    num_labels=num_labels,
    id2label=label_map,
    label2id={label: i for i, label in enumerate(labels)},
    cache_dir=cache_dir,
)

tokenizer = XLMRobertaTokenizerFast.from_pretrained(
    pretrained_model_name_or_path=model_name,
    cache_dir=cache_dir,
    use_fast=True,
)


def tokenize_and_align_labels(examples):
    tokenized_inputs = tokenizer(examples["tokens"], truncation=True, is_split_into_words=True)
    encoded_labels = []
    for i, label in enumerate(examples["encoded"]):
        word_ids = tokenized_inputs.word_ids(batch_index=i)  # Map tokens to their respective word.
        previous_word_idx = None
        label_ids = []
        for word_idx in word_ids:  # Set the special tokens to -100.
            if word_idx is None:
                label_ids.append(-100)
            elif word_idx != previous_word_idx:  # Only label the first token of a given word.
                label_ids.append(label[word_idx])
            else:
                label_ids.append(-100)
            previous_word_idx = word_idx
        encoded_labels.append(label_ids)

    tokenized_inputs["encoded"] = encoded_labels
    return tokenized_inputs


train_tokenized = train.map(tokenize_and_align_labels, batched=True)
train_tokenized = train_tokenized.remove_columns("labels")
train_tokenized = train_tokenized.rename_column("encoded", "labels")
dev_tokenized = dev.map(tokenize_and_align_labels, batched=True)
dev_tokenized = dev_tokenized.remove_columns("labels")
dev_tokenized = dev_tokenized.rename_column("encoded", "labels")
data_collator = DataCollatorForTokenClassification(tokenizer=tokenizer)


model = AutoModelForTokenClassification.from_pretrained(
    model_name,
    config=config,
    cache_dir=cache_dir
)

model_crf = RobertaCRF(config)

"""Trainer"""
training_args = TrainingArguments(
    output_dir="./results",
    evaluation_strategy="epoch", # noqa
    learning_rate=2e-5,
    per_device_train_batch_size=4,
    per_device_eval_batch_size=4,
    num_train_epochs=20,
    weight_decay=0.01,
)

trainer = Trainer(
    model=model_crf,
    args=training_args,
    train_dataset=train_tokenized,
    eval_dataset=dev_tokenized,
    tokenizer=tokenizer,
    data_collator=data_collator,
)

if __name__ == '__main__':
    trainer.train()
