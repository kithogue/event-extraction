import logging
from typing import Optional, Dict, Any, Tuple

import torch
from torch import BoolTensor
from transformers import XLMRobertaForTokenClassification, XLMRobertaConfig, XLMRobertaModel
from TorchCRF import CRF

LOGGER = logging.getLogger(__name__)


def get_last_layer(sequence_outputs: Tuple[torch.Tensor]) -> torch.Tensor:
    """Returns the last tensor of a list of tensors."""
    return sequence_outputs[-1]


POOLERS = {
    'last': get_last_layer,
}


class RobertaForEventClassification(torch.nn.Module):
    """BERT model for NER task.
    The number of NER tags should be defined in the `BertConfig.num_labels`
    attribute.
    Args:
        config: BertConfig instance to build BERT model.
        weight_O: loss weight value for "O" tags in CrossEntropyLoss.
        bias_O: optional value to initiate the classifier's bias value for "O"
            tag.
        pooler: which pooler configuration to use to pass BERT features to the
            classifier.
    """

    def __init__(self,
                 config: XLMRobertaConfig,
                 weight_O: float = 0.01,
                 bias_O: Optional[float] = None,
                 pooler='last'):
        super().__init__()
        # del self.classifier  # Deletes classifier of BertForTokenClassification
        num_labels = config.num_labels  # from RAMS I-, O-, B- ???

        self._build_classifier(config)
        if bias_O is not None:
            self.set_bias_tag_O(bias_O)

        assert isinstance(weight_O, float) and 0 < weight_O < 1
        weights = [1.] * num_labels
        weights[0] = weight_O
        weights = torch.tensor(weights)
        self.loss_fct = torch.nn.CrossEntropyLoss(weight=weights)
        self.pooler = POOLERS.get(pooler)
        self.config = config
        self.model = XLMRobertaModel.from_pretrained('xlm-roberta-base', config=self.config, cache_dir='RAMS/cache')
        self.dropout = torch.nn.Dropout(self.config.hidden_dropout_prob)

    def _build_classifier(self, config):  # use two linear classifiers, one for triggers one for roles ASK
        """Build tag classifier."""
        self.classifier = torch.nn.Linear(config.hidden_size,
                                          config.num_labels)

    def set_bias_tag_O(self, bias_O: Optional[float] = None):  # noqa
        """Increase tag "O" bias to produce high probabilities early on and
        reduce instability in early training."""
        if bias_O is not None:
            LOGGER.info('Setting bias of OUT token to %s.', bias_O)
            self.classifier.bias.data[0] = bias_O

    def bert_encode(self, input_ids, token_type_ids=None, attention_mask=None):
        """Gets encoded sequence from BERT model and pools the layers accordingly.
        BertModel outputs a tuple whose elements are:
        1- Last encoder layer output. Tensor of shape (B, S, H)
        2- Pooled output of the [CLS] token. Tensor of shape (B, H)
        3- Encoder inputs (embeddings) + all Encoder layers' outputs. This
            requires the flag `output_hidden_states=True` on BertConfig. Returns
            List of tensors of shapes (B, S, H).
        4- Attention results, if `output_attentions=True` in BertConfig.
        This method uses just the 3rd output and pools the layers.
        """

        return self.model(
            input_ids,
            token_type_ids=token_type_ids,
            attention_mask=attention_mask
        ).last_hidden_state

    # def bert_encode(self, input_ids, token_type_ids=None, attention_mask=None):
    #     """Gets encoded sequence from BERT model and pools the layers accordingly.
    #     BertModel outputs a tuple whose elements are:
    #     1- Last encoder layer output. Tensor of shape (B, S, H)
    #     2- Pooled output of the [CLS] token. Tensor of shape (B, H)
    #     3- Encoder inputs (embeddings) + all Encoder layers' outputs. This
    #         requires the flag `output_hidden_states=True` on BertConfig. Returns
    #         List of tensors of shapes (B, S, H).
    #     4- Attention results, if `output_attentions=True` in BertConfig.
    #     This method uses just the 3rd output and pools the layers.
    #     """
    #     _, _, all_layers_sequence_outputs, *_ = self.model(
    #         input_ids,
    #         token_type_ids=token_type_ids,
    #         attention_mask=attention_mask)
    #
    #     # Use the defined pooler to pool the hidden representation layers
    #     sequence_output = self.pooler(all_layers_sequence_outputs)
    #
    #     return sequence_output

    def predict_logits(self, input_ids, token_type_ids=None,
                       attention_mask=None):
        """Returns the logits prediction from BERT + classifier."""
        sequence_output = self.bert_encode(
            input_ids, token_type_ids, attention_mask)

        sequence_output = self.dropout(sequence_output)
        logits = self.classifier(sequence_output)  # (batch, seq, tags)

        return logits

    def forward(self,
                input_ids,
                token_type_ids=None,
                attention_mask=None,
                labels=None,
                # prediction_mask=None,
                ) -> Dict[str, torch.Tensor]:
        """Performs the forward pass of the network.
        If `labels` are not None, it will calculate and return the loss.
        Otherwise, it will return the logits and predicted tags tensors.
        Args:
            input_ids: tensor of input token ids.
            token_type_ids: tensor of input sentence type id (0 or 1). Should be
                all zeros for NER. Can be safely set to `None`.
            attention_mask: mask tensor that should have value 0 for [PAD]
                tokens and 1 for other tokens.
            labels: tensor of gold NER tag label ids. Values should be ints in
                the range [0, config.num_labels - 1].
            prediction_mask: mask tensor should have value 0 for tokens that do
                not have an associated prediction, such as [CLS] and WordPìece
                subtoken continuations (that start with ##).
        Returns a dict with calculated tensors:
          - "logits"
          - "y_pred"
          - "loss" (if `labels` is not `None`)
        """
        outputs = {}

        logits = self.predict_logits(input_ids=input_ids,
                                     token_type_ids=token_type_ids,
                                     attention_mask=attention_mask)
        _, y_pred = torch.max(logits, dim=-1)
        y_pred = y_pred.cpu().numpy()
        outputs['logits'] = logits
        outputs['y_pred'] = y_pred

        if labels is not None:
            # Only keep active parts of the loss
            # mask = prediction_mask
            mask: BoolTensor = torch.ones_like(input_ids) == 1
            if mask is not None:
                mask = mask.view(-1)
                active_logits = logits.view(-1, self.config.num_labels)[mask]
                active_labels = labels.view(-1)[mask]
                loss = self.loss_fct(active_logits, active_labels)
            else:
                loss = self.loss_fct(
                    logits.view(-1, self.config.num_labels), labels.view(-1))
            outputs['loss'] = loss

        return outputs


class RobertaCRF(RobertaForEventClassification):
    """BERT-CRF model.
    Args:
        config: BertConfig instance to build BERT model.
        kwargs: arguments to be passed to superclass.
    """

    def __init__(self, config: XLMRobertaConfig, **kwargs: Any):
        super().__init__(config, **kwargs)
        del self.loss_fct  # Delete unused CrossEntropyLoss
        self.crf = CRF(num_labels=config.num_labels)

    def forward(self,
                input_ids,
                token_type_ids=None,
                attention_mask=None,
                labels=None,
                # prediction_mask=None,
                ) -> Dict[str, torch.Tensor]:
        """Performs the forward pass of the network.
        If `labels` is not `None`, it will calculate and return the loss,
        that is the negative log-likelihood of the batch.
        Otherwise, it will calculate the most probable sequence outputs using
        Viterbi decoding and return a list of sequences (List[List[int]]) of
        variable lengths.
        Args:
            input_ids: tensor of input token ids.
            token_type_ids: tensor of input sentence type id (0 or 1). Should be
                all zeros for NER. Can be safely set to `None`.
            attention_mask: mask tensor that should have value 0 for [PAD]
                tokens and 1 for other tokens.
            labels: tensor of gold NER tag label ids. Values should be ints in
                the range [0, config.num_labels - 1].
            prediction_mask: mask tensor should have value 0 for tokens that do
                not have an associated prediction, such as [CLS] and WordPìece
                subtoken continuations (that start with ##).
        Returns a dict with calculated tensors:
          - "logits"
          - "loss" (if `labels` is not `None`)
          - "y_pred" (if `labels` is `None`)
        """
        outputs = {}

        logits = self.predict_logits(input_ids=input_ids,
                                     token_type_ids=token_type_ids,
                                     attention_mask=attention_mask)
        outputs['logits'] = logits

        # mask: mask padded sequence and also subtokens, because they must
        # not be used in CRF.
        # mask = prediction_mask

        mask = torch.ones_like(input_ids) == 1
        batch_size = logits.shape[0]

        if labels is not None:
            # Negative of the log likelihood.
            # Loop through the batch here because of 2 reasons:
            # 1- the CRF package assumes the mask tensor cannot have interleaved
            # zeros and ones. In other words, the mask should start with True
            # values, transition to False at some moment and never transition
            # back to True. That can only happen for simple padded sequences.
            # 2- The first column of mask tensor should be all True, and we
            # cannot guarantee that because we have to mask all non-first
            # subtokens of the WordPiece tokenization.
            # for seq_logits, seq_labels, seq_mask in zip(logits, labels, mask):
                # # Index logits and labels using prediction mask to pass only the
                # # first subtoken of each word to CRF.
                # seq_logits = seq_logits[seq_mask].unsqueeze(0)
                # seq_labels = seq_labels[seq_mask].unsqueeze(0)
                # loss -= self.crf(seq_logits, seq_labels,
                #                  reduction='token_mean')
            losses = self.crf(logits, labels, mask)
            loss = 0
            for _loss in losses:
                loss -= _loss
            loss /= batch_size
            outputs['loss'] = loss

        else:
            # # Same reasons for iterating
            # output_tags = []
            # for seq_logits, seq_mask in zip(logits, mask):
            #     seq_logits = seq_logits[seq_mask].unsqueeze(0)
            #     tags = self.crf.decode(seq_logits)
            #     # Unpack "batch" results
            #     output_tags.append(tags[0])

            outputs['y_pred'] = self.crf.viterbi_decode(logits, mask)

        return outputs
