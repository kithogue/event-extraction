import datasets
import numpy as np
import pandas as pd
import jsonlines
from typing import List, Dict, Tuple, Union
import random
import re
from datasets import load_dataset, ClassLabel, Features, Value, DatasetDict, Dataset, IterableDatasetDict, \
    IterableDataset
from sklearn.preprocessing import LabelEncoder


def line_to_play_with(path: str) -> Dict:
    with jsonlines.open(path) as reader:
        data = [obj for obj in reader]
        index = random.randint(0, len(data) - 1)
        return data[index]


class DatasetReader:
    def __init__(self):
        self.train_path = "/home/ndazhunts/CLARIN/event_detection/RAMS/data/RAMS_1.0/data/train.jsonlines"
        self.test_path = "/home/ndazhunts/CLARIN/event_detection/RAMS/data/RAMS_1.0/data/test.jsonlines"
        self.dev_path = "/home/ndazhunts/CLARIN/event_detection/RAMS/data/RAMS_1.0/data/dev.jsonlines"
        self.train_out = '/home/ndazhunts/CLARIN/event_detection/RAMS/data/clean/train_enc.jsonl'
        self.dev_out = '/home/ndazhunts/CLARIN/event_detection/RAMS/data/clean/dev_enc.jsonl'
        self.test_out = '/home/ndazhunts/CLARIN/event_detection/RAMS/data/clean/test_enc.jsonl'

    def __flatten(self, source_list: List) -> List:
        if not any(isinstance(i, list) for i in source_list):
            return source_list
        else:
            return [item for sublist in source_list for item in sublist]

    def __match_role_name(self, role_name: str) -> str:
        return re.sub(pattern=r'^evt\d{2,3}arg\d{2,3}',
                      repl='',
                      string=role_name)

    def __combine_roles_and_triggers(self, roles: List, triggers: List) -> List:
        trigger_flat = self.__flatten(triggers)
        trigger_name = self.__flatten(trigger_flat[2])[0]
        result = [(r[1], self.__match_role_name(r[2])) for r in roles]
        result.append(([trigger_flat[0], trigger_flat[1]], trigger_name + '.trigger'))
        return result

    def __is_not_in_spans(self, targets: List, index: int) -> bool:
        check_spans = 0
        for t in targets:
            if index in range(t[0][0], t[0][1] + 1):
                check_spans = check_spans + 1
        return check_spans == 0

    def parse_to_conll(self, sentences: List, targets: List) -> List:
        result = []
        for i in range(len(sentences)):
            if self.__is_not_in_spans(targets, i):
                result.append((sentences[i], 'O'))
            else:
                for t in targets:
                    if i == t[0][0]:
                        target = ''.join(['B-', t[1]])
                        result.append((sentences[i], target))
                    elif t[0][0] < i <= t[0][1]:
                        target = ''.join(['I-', t[1]])
                        result.append((sentences[i], target))
        result.append((None, None))
        return result

    def parse_to_toks_and_labels(self, sentences: List, targets: List) -> Tuple[List, List]:
        toks = []
        labels = []
        for i in range(len(sentences)):
            if self.__is_not_in_spans(targets, i):
                toks.append(sentences[i])
                labels.append('O')
            else:
                for t in targets:
                    if i == t[0][0]:
                        target = ''.join(['B-', t[1]])
                        toks.append(sentences[i])
                        labels.append(target)
                    elif t[0][0] < i <= t[0][1]:
                        target = ''.join(['I-', t[1]])
                        toks.append(sentences[i])
                        labels.append(target)
        assert len(toks) == len(labels)
        return toks, labels

    def __read_to_df(self, path: str) -> pd.DataFrame:
        result = []
        with jsonlines.open(path) as reader:
            data = [obj for obj in reader]
            for line in data:
                sentences = self.__flatten(line['sentences'])
                roles = line['gold_evt_links']
                trigger = line['evt_triggers']
                combined = self.__combine_roles_and_triggers(roles=roles, triggers=trigger)
                entities = self.parse_to_conll(sentences=sentences, targets=combined)
                sequence = pd.DataFrame(entities, columns=['Token', 'Target'])
                result.append(sequence)
        result = pd.concat(result, ignore_index=True)
        return result

    def get_dictionary(self) -> np.ndarray:
        df_train = self.__read_to_df(self.train_path)
        df_dev = self.__read_to_df(self.dev_path)
        df_test = self.__read_to_df(self.test_path)
        classes_train = self.__get_classes(df_train)
        classes_dev = self.__get_classes(df_dev)
        classes_test = self.__get_classes(df_test)
        train_dev_classes = np.union1d(classes_train, classes_dev)
        return np.union1d(train_dev_classes, classes_test)

    def write_to_jsonl(self, path_in: str, path_out: str) -> None:
        labels_dict = self.get_dictionary()
        label_encoder = LabelEncoder()
        label_encoder.fit(labels_dict)
        with jsonlines.open(path_out, mode='w') as writer:
            with jsonlines.open(path_in) as reader:
                for line in reader:
                    sentences = self.__flatten(line['sentences'])
                    roles = line['gold_evt_links']
                    trigger = line['evt_triggers']
                    combined = self.__combine_roles_and_triggers(roles=roles, triggers=trigger)
                    toks, labels = self.parse_to_toks_and_labels(sentences, combined)
                    encoded = label_encoder.transform(labels)
                    encoded = [int(label) for label in encoded]
                    json_dict = {"tokens": toks, "labels": labels, "encoded": encoded}
                    writer.write(json_dict)

    def __get_classes(self, input_df: pd.DataFrame) -> np.ndarray:
        result = input_df.Target.unique()
        return np.array([target for target in result if target is not None])

    def prepare_datasets(self):
        return load_dataset('json', data_files={'train': self.train_out, 'dev': self.dev_out, 'test': self.test_out})

    def __add_encoded_label(self, labels: List[str]) -> List[int]:
        labels_dict = self.get_dictionary()
        label_encoder = LabelEncoder()
        label_encoder.fit(labels_dict)
        transformed = label_encoder.transform(labels)
        return [int(label) for label in transformed]

    def __add_encoded(self, ds):
        targets = ds['labels']
        encoded = self.__add_encoded_label(targets)
        ds = ds.add_column("encoded", encoded)
        return ds


if __name__ == '__main__':
    ds_reader = DatasetReader()
    # ds_reader.write_to_jsonl(ds_reader.train_path, ds_reader.train_out)
    # ds_reader.write_to_jsonl(ds_reader.dev_path, ds_reader.dev_out)
    ds_reader.write_to_jsonl(ds_reader.test_path, ds_reader.test_out)
